struct Interval {
    number: u32,
    bottom: u32,
    top: u32
}

fn bound(k: u32) -> u32 {
    let p = 10_u32.pow(k);
    (k * p) - (p.saturating_sub(10)) / 9
}

fn build_interval(n: u32) -> Interval {
    Interval {
        number: n + 1,
        bottom: bound(n),
        top: bound(n + 1)
    }
}

fn interval_of(d: u32) -> Option<Interval> {
    for i in 0 .. u32::max_value() {
        let itv = build_interval(i);
        if d < itv.top {
            return Some(itv);
        }
    }
    return None;
}

fn digit_value(pos: u32) -> u32 {
    let itv = interval_of(pos).unwrap();
    let length = pos - itv.bottom;
    let offset = itv.number - (length % itv.number) - 1;
    let length = length / itv.number;
    let base = 10_u32.pow(itv.number - 1);
    let value = if base == 1 { length } else { base + length };
    value / 10_u32.pow(offset) % 10
}

fn main() {
    println!("{}",
        digit_value(1) *
        digit_value(10) *
        digit_value(100) *
        digit_value(1000) *
        digit_value(10000) *
        digit_value(100000) *
        digit_value(1000000)
    );
}
