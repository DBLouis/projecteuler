use std::fs;

const WORDS_FILE: &str = "words.txt";

fn triangle(n: u32) -> u32 {
    return (n * (n + 1)) / 2;
}

fn word_value(word: &str) -> u32 {
    return word.chars()
        .map(|c| match c {
            'A'...'Z' => (c as u32) - ('A' as u32) + 1,
            'a'...'z' => (c as u32) - ('a' as u32) + 1,
            _ => 0,
        })
        .sum();
}

fn split_words(text: &str) -> Vec<&str> {
    text.split("\n").collect()
}

fn words_to_values(words: Vec<&str>) -> Vec<u32> {
    let mut values: Vec<u32> = words.iter().map(|w| word_value(w)).collect();
    values.sort_unstable();
    return values;
}

fn count_triangle_words(values: Vec<u32>) -> u32 {
    let mut count = 0;
    let mut n = 1;
    for v in values {
        while triangle(n) < v {
            n += 1;
        }
        if v == triangle(n) {
            count += 1;
        }
    }
    return count;
}

fn main() {
    match fs::read_to_string(WORDS_FILE) {
        Ok(text) => {
            let words = split_words(&text);
            let values = words_to_values(words);
            println!(
                "Number of triangle words is: {}",
                count_triangle_words(values)
            )
        }
        Err(e) => println!("{}", e),
    }
}
