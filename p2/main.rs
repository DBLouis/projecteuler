const MAX: u32 = 4_000_000;

fn fib_even_sum() -> u32 {
    let mut s = 0;
    let mut a = 1;
    let mut b = 2;
    while a <= MAX {
        if a % 2 == 0 {
            s += a;
        }
        b = a + b;
        a = b - a;
    }
    return s;
}

fn main() {
    println!("Result is {}", fib_even_sum());
}
